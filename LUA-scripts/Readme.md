### PCA9685 Driver for Ardupilot

  

This sample script contains everything to access the PCA9685 16-Channel PWM via I2C (https://www.bastelgarage.ch/16-kanal-pwm-servo-treiber-i2c-pca9685)

  

**Warning: This script works but wasn't fully tested!**

  

The script will be called every 1000ms by Ardupilot. This time can be configured by changing the return value:

  
  

```lua

return update, 1000  -- reschedules the loop

```

  

The default address is ``0x40`` and the default I2C-Interface is ``1``. These values can be changed on these lines:

  

```lua

-- User configuration
local address = 0x40
local i2c_bus_number = 1
-- User configuration end

```

  

Place your "application logic" inside the `function update()` method:

  

```lua

function  update() -- this is the loop which periodically runs
	-- PLACE CODE HERE
	return update, 1000  -- reschedules the loop
end

```

  

Usually you only need the ``function set_pwm_dutycycle(channel, value)`` and ``function set_pwm_frequency(value)`` method.

  

``function set_pwm_dutycycle(channel, value)``

  

Sets the duty-cycle (0 - 4095) for a specified channel (0-15)

  

``function set_pwm_frequency(value)``

  

Sets the global PWM frequency (24 - 1526). All channels share the same value.