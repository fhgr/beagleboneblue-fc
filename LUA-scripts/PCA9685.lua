-- User configuration
local address = 0x40
local i2c_bus_number = 1
-- User configuration end
 
local i2c_bus = i2c.get_device(i2c_bus_number, 0)
i2c_bus:set_retries(10)
i2c_bus:set_address(address)
 
local OSCILLATOR_CLOCK = 25000000
 
local Registers = {
    MODE_1 = 0x00,
    MODE_2 = 0x01,
    CHANNEL_START = 0x08,
    PRESCALE = 0xFE
}
 
local Mode1 = {
    RESTART = 7,
    EXTCLK = 6,
    AI = 5,
    SLEEP = 4,
    SUB1 = 3,
    SUB2 = 2,
    SUB3 = 1,
    ALLCALL = 0
}
 
local Mode2 = {
    INVRT = 4,
    OCH = 3,
    OUTDRV = 2,
    OUTNE_1 = 1,
    OUTNE_0 = 0
}
 
local state = 0
 
function value_low(value)
    return value & 0xFF
end
 
function value_high(value)
    return (value >> 8) & 0xFF
end
 
function read_mode_1()
    return i2c_bus:read_registers(Registers.MODE_1)
end
 
function calculate_channel_address(channel)
    return Registers.CHANNEL_START + (channel * 4)
end
 
function read_channel_value(channel)
    local register_low = calculate_channel_address(channel)
    local register_high = register_low + 1
    local value_low = i2c_bus:read_registers(register_low)
    local value_high = i2c_bus:read_registers(register_high)
    return value_low | (value_high << 8)
end
 
function read_pwm_frequency()
    local prescale = i2c_bus:read_registers(Registers.PRESCALE)
    return calculate_frequency(prescale)
end
 
function read_pwm_prescale()
    local prescale = i2c_bus:read_registers(Registers.PRESCALE)
    return prescale
end
 
function calculate_frequency(prescale)
    return math.floor((OSCILLATOR_CLOCK / ((prescale + 1) * 4096.0) + 0.5))
end
 
function calculate_prescale(frequency)
    return math.floor((OSCILLATOR_CLOCK / (4096 * frequency) - 1) + 0.5)
end
 
function set_pwm_dutycycle(channel, value)
 
    if (channel < 0 or channel > 15) then return end
 
    if (value < 0 or value > 4095) then return end
 
    local register = Registers.CHANNEL_START + (channel * 4)
    local value_low = value_low(value)
    local value_high = value_high(value)
 
    i2c_bus:write_register(register, value_low)
    i2c_bus:write_register(register + 1, value_high)
 
end
 
function set_pwm_frequency(value)
 
    if (value < 24 or value > 1526) then return end
 
    local prescale = calculate_prescale(value)
    sleep()
    i2c_bus:write_register(Registers.PRESCALE, prescale)
    wake()
end
 
function sleep()
    i2c_bus:write_register(Registers.MODE_1, read_mode_1() | (1 << Mode1.SLEEP))
end
 
function wake()
    i2c_bus:write_register(Registers.MODE_1, read_mode_1() & (255 - (1 << Mode1.SLEEP)))
end
 
function update() -- this is the loop which periodically runs
 
    if not i2c_bus:read_registers(0) then
        gcs:send_text(0, "PCA9685 not found at address " .. tostring(address))
        return update, 1000
    end
 
    state = state + 1
 
    set_pwm_dutycycle(0, state * 100)
    gcs:send_text(0, "PWM Channel 0 is: " .. tostring(read_channel_value(0)))
    set_pwm_frequency(600)
    gcs:send_text(0, "PWM Frequency is: " .. tostring(read_pwm_frequency()))
 
 
    if state == 6 then
        state = 0
    end
 
    return update, 1000 -- reschedules the loop
end
 
return update() -- run immediately before starting to reschedule
 
 