# BeagleBoneBlue FC

Wir beschreiben in diesem Projekt die nötigen Schritte um einen BeagleBoneBlue als ArduPilot FC zu konfigurieren.

Dieser FC wird aktuell im Projekt "Autonomes Segelfliegen" verwendet, später können weitere folgen.

# Setting up Ardupilot on BeagleBone Blue

## Preparing BeagleBone Blue

Download the latest buster-console image from https://rcn-ee.net/rootfs/bb.org/testing/ ([bone-debian-10.11-console-armhf-2021-12-01-1gb.img.xz](https://rcn-ee.net/rootfs/bb.org/testing/2021-12-01/buster-console/bone-debian-10.11-console-armhf-2021-12-01-1gb.img.xz) used in this example)

Flash image onto SD card

Insert SD card in BBB and power up using a suitable USB cable at the micro USB bus.

Connect via SSH (`ssh debian@192.168.7.2` on Windows and `ssh debian@192.168.6.2` on Mac/Linux)
The password should be 'temppwd'.
It is given at the command prompt.
Eliminate the necessity for the user to enter the sudoer password.

```
$ echo "debian ALL=(ALL) NOPASSWD: ALL" | sudo tee -a /etc/sudoers.d/debian >/dev/null
```
Install locales (a lot of programs complain otherwise) and set them
```
$ sudo apt -get install -y locales
$ sudo dpkg-reconfigure locales
```
Choose a locale (e.g. German, Switzerland, UTF8)
This may take a while.

Connect BBB to the internet (see chapter [Connect BBB to the internet](#connect-bbb-to-the-internet) )

```
$ sudo apt-get -y update
$ sudo apt-get -y dist-upgrade
$ sudo apt-get install -y git
Not necessary (it's already installed): $ sudo apt-get install -y cpufrequtils
```
Update locat scripts
```
$ cd /opt/scripts && git pull
```
Specify device tree binary to be used at startup.
```
$ sudo sed -i 's/#dtb=/dtb=am335x-boneblue.dtb/g' /boot/uEnv.txt
```

```
$ sudo nano /boot/uEnv.txt
```

Make sure that `uboot_overlay_pru=AM335X-PRU-RPROC-4-19-TI-00A0.dtbo`

```
Not necessary (it's already on "performance"): $ sudo sed -i 's/GOVERNOR="ondemand"/GOVERNOR="performance"/g' /etc/init.d/cpufrequtils
$ sudo reboot
```

## Setting up Ardupilot

There are several sources available for Ardupilot binaries. Please refer to [Prebuilt Binaries and ready to use Images](#prebuilt-binaries-and-ready-to-use-images)


Create an empty service file so that ardupilot automatically starts on boot and runs in the background. 

`$ sudo nano /lib/systemd/system/arduplane.service`

Paste following text. And replace `<target IP address>` with the IP address of the telemetry receiving computer

```
[Unit]
Description=ArduPlane Service
After=networking.service
StartLimitIntervalSec=0
Conflicts=arducopter.service ardurover.service antennatracker.service

[Service]
ExecStart=/usr/bin/ardupilot/arduplane -C /dev/ttyO1 -A udp:<target IP address>:14550 -B /dev/ttyS2
Restart=on-failure
RestartSec=1

[Install]
WantedBy=multi-user.target
```

```
$ sudo mkdir -p /usr/bin/ardupilot
```

Download from https://firmware.ardupilot.org/ (or build yourself) the ArduPlane binary and copy it to `/usr/bin/ardupilot`

```
$ sudo chmod 0755 /usr/bin/ardupilot/a*
$ sudo systemctl enable arduplane.service
$ sudo reboot
```

## Flash SD card to eMMC

You can use the following steps to copy everything over to the eMMC. This way the SD card can be removed or used for other purposes.

```
$ sudo nano /boot/uEnv.txt
```
Uncomment the line `#cmdline=init=/opt/scripts/tools/eMMC/init-eMMC-flasher-v3.sh`

It should now be:

```
cmdline=init=/opt/scripts/tools/eMMC/init-eMMC-flasher-v3.sh
```

Save the file and `sudo reboot`

The flashing process will immediately start on reboot. **This process can take up 15 minutes (Usually 5 minutes). Wait until all LEDs are OFF**

**Once done, remove power and then remove the SD card**

Power it up again and it should boot as usual

## Connect BBB to the internet

### Via Wifi

Connect the BBBlue to an available access point - this does NOT setup the BBBlue to act as access point itself.

```
$ connmanctl services | grep 'YOUR_SSID' | grep -Po 'wifi_[^ ]+'
$ cat >/var/lib/connman/wifi.config
```

one line at a time, we're writing a file line by line

```
[service_<OUTPUT-FROM-CONNMANCTL-COMMAND>]
Type = wifi
Security = wpa2
Name = YOUR_SSID
Passphrase = YOUR_WIFI_PASSWORD
```

CTRL + C, should save File contents. Make sure you pressed Enter after Passphrase = ***** 

```
$ sudo reboot
```
Again, ssh into the BeagleBoneBlue at the former IP.
Obtain the IP of the BeagleBoneBlue wifi interface
```
$ ip addr
```
ssh by wifi into the BeagleBoneBlue

### Via shared internet connection

This setting is non-permanent and has to be done after each boot of the BBBlue.

BBBlue's command prompt:
```
$ sudo /sbin/route add default gw 192.168.7.1
$ echo -e "nameserver 8.8.8.8\nnameserver 1.1.1.1" | sudo tee -a /etc/resolv.conf >/dev/null
```
Host computer's command prompt:
```
$ ifconfig
```
use this output to figure out which is your internet connected network adapter and replace in the following 'wlan0' by the name of it:
```
$ sudo sysctl net.ipv4.ip_forward=1
$ sudo iptables -t nat -A POSTROUTING -o wlan0 -j MASQUERADE
$ sudo iptables -A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
$ sudo iptables -A FORWARD -i usb0 -o wlan0 -j ACCEPT
```
in order to test the connection go to the BBBlue's command prompt:
```
$ ping -c 3 google.com
```

## Troubleshooting

### RCOutputAioPRU.cpp:SIGBUS error generated

https://github.com/imfatant/test/issues/16#issuecomment-477725439

For me the `sudo dd if=/dev/zero of=/dev/mmcblk1 bs=1M count=10` did the trick

Careful: The uboot_overlay_pru version in this Github comment is outdated. Use the instructions above for reference

### Check if booted from eMMC or SD card

```
$ sudo apt-get install tiomapconf
```

Boot from eMMC:
```
$ sudo omapconf read 0x44E10040
0040033C
```
Boot from microSD (boot button pressed):

```
$ sudo omapconf read 0x44E10040
00400338
```

### Check system config

```
$ sudo /opt/scripts/tools/version.sh
```

The "current" config should look like this (Use diffchecker or similar tool):

```
git:/opt/scripts/:[1583f354594aabfaff08dee2a4aabdfe61433024]
eeprom:[A335BNLTBLA21736EL001182]
model:[TI_AM335x_BeagleBone_Blue]
dogtag:[BeagleBoard.org Debian Buster Console Image 2021-12-01]
bootloader:[microSD-(push-button)]:[/dev/mmcblk0]:[U-Boot SPL 2019.04-g923f8b8 (Oct 26 2021 - 14:46:57 +0000)]:[location: dd MBR]
bootloader:[microSD-(push-button)]:[/dev/mmcblk0]:[U-Boot 2019.04-g923f8b8]:[location: dd MBR]
bootloader:[eMMC-(default)]:[/dev/mmcblk1]:[U-Boot SPL 2019.04-g923f8b8 (Oct 26 2021 - 14:46:57 +0000)]:[location: dd MBR]
bootloader:[eMMC-(default)]:[/dev/mmcblk1]:[U-Boot 2019.04-g923f8b8]:[location: dd MBR]
UBOOT: Booted Device-Tree:[am335x-boneblue.dts]
UBOOT: Loaded Overlay:[AM335X-PRU-RPROC-4-19-TI-00A0.kernel]
UBOOT: Loaded Overlay:[BB-ADC-00A0.kernel]
kernel:[4.19.94-ti-r68]
device-tree-override:[dtb=am335x-boneblue.dtb]
/boot/uEnv.txt Settings:
uboot_overlay_options:[enable_uboot_overlays=1]
uboot_overlay_options:[uboot_overlay_pru=AM335X-PRU-RPROC-4-19-TI-00A0.dtbo]
uboot_overlay_options:[enable_uboot_cape_universal=1]
pkg check: to individually upgrade run: [sudo apt install --only-upgrade <pkg>]
pkg:[bb-cape-overlays]:[4.14.20210821.0-0~buster+20210821]
pkg:[bb-customizations]:[1.20211201.1-0~buster+20211201]
pkg:[bb-usb-gadgets]:[1.20211012.0-0~buster+20211012]
pkg:[bb-wl18xx-firmware]:[1.20210922.2-0~buster+20211007]
pkg:[kmod]:[26-1]
WARNING:pkg:[librobotcontrol]:[NOT_INSTALLED]
pkg:[firmware-ti-connectivity]:[20190717-2rcnee1~buster+20200305]
groups:[debian : debian adm kmem dialout cdrom floppy audio dip video plugdev users systemd-journal input bluetooth netdev gpio admin tisdk weston-launch cloud9ide]
cmdline:[console=ttyS0,115200n8 bone_capemgr.uboot_capemgr_enabled=1 root=/dev/mmcblk0p1 ro rootfstype=ext4 rootwait coherent_pool=1M net.ifnames=0 lpj=1990656 rng_core.default_quality=100 quiet]
dmesg | grep remote
[   26.835790] remoteproc remoteproc0: wkup_m3 is available
[   26.901553] remoteproc remoteproc0: powering up wkup_m3
[   26.901586] remoteproc remoteproc0: Booting fw image am335x-pm-firmware.elf, size 217148
[   26.901840] remoteproc remoteproc0: remote processor wkup_m3 is now up
[   28.464026] remoteproc remoteproc1: 4a334000.pru is available
[   28.480195] remoteproc remoteproc2: 4a338000.pru is available
dmesg | grep pru
[   28.464026] remoteproc remoteproc1: 4a334000.pru is available
[   28.464212] pru-rproc 4a334000.pru: PRU rproc node pru@4a334000 probed successfully
[   28.480195] remoteproc remoteproc2: 4a338000.pru is available
[   28.480412] pru-rproc 4a338000.pru: PRU rproc node pru@4a338000 probed successfully
dmesg | grep pinctrl-single
[    0.975434] pinctrl-single 44e10800.pinmux: 142 pins, size 568
dmesg | grep gpio-of-helper
END
```

### Prebuilt Binaries and ready to use Images

https://drive.switch.ch/index.php/s/B0TXf6GkaY6BTGl

https://firmware.ardupilot.org/Plane/stable-4.1.6/blue/


# Weitere Informationen

Main source of information: https://ardupilot.org/dev/docs/building-for-beaglebone-black-on-linux.html#building-for-beaglebone-black-on-linux

Wfif connection: https://beagleboard.org/static/librobotcontrol/networking_wifi.html

Setting up: https://muonic.wordpress.com/2019/04/09/setting-up-the-beagleboneblue-for-the-first-time/

Installing debian: https://elinux.org/Beagleboard:BeagleBoneBlack_Debian#Flashing_eMMC

BeagleBone Blue Pinouts and additional Wiki: https://github.com/beagleboard/beaglebone-blue/wiki/Pinouts

Es existieren mehrere Anleitungen dazu, leider sind viele von 2018 und damit veraltet:

- ArduPilot Anleitung: https://ardupilot.org/copter/docs/common-beagle-bone-blue.html
    - verweist primär auf: https://github.com/mirkix/ardupilotblue
        - verweist wieder auf: https://github.com/imfatant/test
            - enthält tote Links und veraltete Binaries, siehe: https://github.com/ArduPilot/ardupilot/issues/18921

Die aktuellsten Anleitungen derzeit scheinen diese zu sein:
- [03_GettingStartedBBBL.pdf](03_GettingStartedBBBL.pdf) von https://inst.eecs.berkeley.edu/~ee192/sp21/files/GettingStartedBBBL.pdf
- [04_Sudo_Null_IT_News.pdf](04_Sudo_Null_IT_News.pdf) von https://sudonull.com/post/8513-ArduPilot-for-beginners-Installation-and-configuration-on-BeagleBone-Blue


# Appendix

## Cross compiling ArduPilot from source

The binaries can be found on https://github.com/drtrigon/ardupilot-beagle-bone-blue-binaries and are built according to the procedure described in the following. This workflow setup on github can be used in order to automate building of forked code in a simple and easy manner and by that enabling all colaborators on the fork to use the same binaries.

Install Ubuntu 20.04 64-Bit as build machine (Can be VM)

```
$ sudo apt update
$ sudo apt install git
$ git clone https://github.com/ardupilot/ardupilot.git
$ cd ardupilot
```

use either the stable tag `ArduPlane-stable` or the current Plane branch `Plane-4.1` (in the moment both refer to Plane-4.1.6) - make sure to use a `stable` Version

```
$ ./Tools/environment_install/install-prereqs-ubuntu.sh
$ git checkout Plane-4.1
$ git submodule update --init --recursive
```

```
$ ./waf configure --board=blue
$ ./waf plane
```

binaries are located in `build/blue/bin/`

Hint: For reference is this procedure also documented and used on https://github.com/drtrigon/ardupilot-beagle-bone-blue-binaries/ (used to be automatically built once a day).



# Tests

1. [test_01.txt](test_01.txt) anhand von 03_GettingStartedBBBL.pdf
2. (... anhand von 04_Sudo_Null_IT_News.pdf ... ?)


